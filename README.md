# Endeev Frontend Assessment

## What do you have to do?
An app to show IoT devices in a city. These devices will have states, locations and zones. The states will be ON and OFF. Location will be latitude and longitude. There are three zones (Norte, Sur and Centro).

## Pages
1. Home / Dashboard.
2. List of Devices.
3. Add a new Device.

### Home / Dashboard
In this page you need to show how many devices are ON and OFF. Also, you need to display some charts to show data of the devices.

### List of Devices
This page should include the list of all devices. The user should be able to filter by device name, states and zones.

### Add a new Device
Form with validation to create a new Device. This form should save the data with the endpoints.

## Code
You need to fork this repository in order to complete the exercise.

#### Commands
* Install dependencies: ```npm install```
* Init the project: ```npm run start```
* Init the database from Json file: ```npm run server```

#### Endpoints
* List all devices: ```GET http://localhost:3001/devices```
* List one device: ```GET http://localhost:3001/devices/:ID```
* Add a new device:  ```POST http://localhost:3001/devices```

Post Data example:
 ```
{
     "lat": -31.656403,
     "lng": -60.713565,
     "zone": "Norte",
     "state": 1
}
```

## Clarifications
* Global states to show the states of the devices.
* Use a chart library.
* Json with devices data is in the repository.
* Use the last LTS version of NodeJs (today ```v10.15.1```).
* You are able to use any library for prebuild components (reactstrap, materialize, etc).

## Extra (not mandatory)
* You can add a map and show the devices on it.
(In case you want to use Google Maps and you don't have an apikey, ask for the apikey to us).
* You can add the edition of the devices.
* Documentation (in English).
* Use git flow.
* Use Docker.