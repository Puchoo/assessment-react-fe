import React, {useState} from 'react';
import {connect} from "react-redux";
import {loginWithGitHub, loginWithGoogle} from "../../redux/actions";
import * as firebase from "firebase";
import {Alert, Button, Card, Form, Icon, Input} from "antd";

const _login = (props) => {

    const [error, setError] = useState({errorCode: 0, errorMessage: ''});

    const handlerGitHubLogin = () => {
        const provider = new firebase.auth.GithubAuthProvider();
        loginWithFirebasePopup(provider, props.loginWithGitHub);
    };

    const handlerGoogleLogin = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().useDeviceLanguage();
        loginWithFirebasePopup(provider, props.loginWithGoogle);
    };

    const loginWithFirebasePopup = (provider, loginSuccess) => {

        firebase.auth().signInWithPopup(provider).then(function (result) {
            const token = result.credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            loginSuccess(token, user);
        }).catch(function (error) {
            const errorCode = error.code;
            const errorMessage = error.message;
            setError({errorCode, errorMessage});
            // The email of the user's account used.
            const email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            const credential = error.credential;
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) console.log('This type of login is not implemented sorry ^^');
        });
    };

    return (
        <Card
            title="Login"
            style={{width: 300, margin: 'auto', marginTop: '5rem'}}
            actions={[
                <div onClick={() => handlerGitHubLogin()}> <Icon type="github"/> </div>,
                <div onClick={() => handlerGoogleLogin()}> <Icon type="google"/> </div>,
            ]}
        >
            <Form onSubmit={handleSubmit} className="login-form">
                <Form.Item>
                    {props.form.getFieldDecorator('userName', {
                        rules: [{required: true, message: 'Please input your username!'}],
                    })(
                        <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>} placeholder="Username"/>
                    )}
                </Form.Item>
                <Form.Item>
                    {props.form.getFieldDecorator('password', {
                        rules: [{required: true, message: 'Please input your Password!'}],
                    })(
                        <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                               placeholder="Password"/>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                </Form.Item>
            </Form>
            {error.errorCode > 0 &&
            <Alert
                message={error.errorCode}
                description={error.errorMessage}
                type="error"
                closable
            />
            }
        </Card>
    );
};

function mapStateToProps(state) {
    return {
        ...state,
    }
}

const Login = Form.create({name: 'normal_login'})(_login);

export default connect(mapStateToProps, {loginWithGitHub, loginWithGoogle})(Login);

