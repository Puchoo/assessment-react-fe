import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import {fetchDevices, logoutUser} from "../../redux/actions";
import {Avatar, Icon, Menu} from "antd";

import history from "../../utils/history";

class Header extends Component {

    handleClick = (e) => {
        const {logoutUser} = this.props;
        if (e.key === 'logout') logoutUser();
        else history.push('/' + e.key);
    };

    componentDidMount() {
        // Get all devices from the server
        this.props.fetchDevices()
    }

    render() {
        const {user} = this.props;
        const {location} = history;
        return (
            <Menu
                onClick={this.handleClick}
                selectedKeys={[location.pathname.replace('/','')]}
                mode="horizontal"
            >
                <Menu.Item key="home" style={{float: 'left'}}>
                    <Icon type="home"/>Home
                </Menu.Item>
                <Menu.Item key="detail" style={{float: 'left'}}>
                    <Icon type="dot-chart"/>List of Devices
                </Menu.Item>
                <Menu.Item key='logout' style={{float: 'right'}}>
                    <Avatar size="large" src={user.photoURL} style={{marginRight: '0.5rem'}} />
                    Logout
                </Menu.Item>
            </Menu>
        );
    }
}


const mapStateToProps = (state) => {
    const {user} = state.auth;

    return {user};
};


export default withRouter(connect(mapStateToProps, {logoutUser, fetchDevices})(Header));