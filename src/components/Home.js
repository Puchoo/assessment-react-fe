import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {Alert, Icon, Layout, Menu,} from "antd";
import {fetchDevices} from "../redux/actions";
import ChartView from "./charts/ChartView";
import MapView from "./MapView";

const {SubMenu} = Menu;
const {Sider, Content} = Layout;

class Home extends Component {

    state = {
        selectedKey: '1'
    };

    renderMenu() {
        return (
            <Menu
                style={{width: 200}}
                onClick={(e) => this.handleClick(e)}
                defaultSelectedKeys={['1']}
                mode='vertical'
                theme='light'
            >
                <Menu.Item key="1">
                    <Icon type="pie-chart"/>
                    Chart View
                </Menu.Item>
                <Menu.Item key="2">
                    <Icon type="environment"/>
                    Map View
                </Menu.Item>
                <SubMenu key="sub1"
                         title={<span><Icon type="appstore"/><span>More Charts</span></span>}
                >
                    <Menu.Item key="3">Some other no implemented option</Menu.Item>
                    <Menu.Item key="4">Some other no implemented option</Menu.Item>
                </SubMenu>
            </Menu>
        )
    }

    handleClick(e) {
        this.setState({selectedKey: e.key})
    }

    render() {
        const {selectedKey} = this.state;

        return (
            <Layout style={{height: '100vh'}}>
                <Sider theme='light ' width={200}>{this.renderMenu()}</Sider>
                <Content>
                    <Alert
                        style={{width: '75%', margin: ' auto', marginTop: '0.5rem'}}
                        message="Requirement"
                        description="In this page you need to show how many devices are ON and OFF. Also, you need to display some charts to show data of the devices."
                        type="info" showIcon closable/>

                    {selectedKey === '1' &&
                    <ChartView/>
                    }
                    {selectedKey === '2' &&
                    <MapView/>
                    }
                </Content>
            </Layout>
        );


    }

}

const mapStateToProps = (state) => {
    return {...state};
};


export default withRouter(connect(mapStateToProps, {})(Home));
