import React, {Component, useState} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import _ from "lodash";
import {deleteDevice, editDevice, newDevice} from "../redux/actions";
import {Alert, Button, Col, Form, Icon, Input, List, Menu, Modal, Row, Dropdown} from "antd";
import {googleMapsApiKey} from "../env";


class Detail extends Component {

    state = {
        modalOpen: false,
        editMode: false,
        deviceToEdit: {},
        indexOfEditedDevice: -1,
        devices: this.props.data.devices || []
    };

    componentWillReceiveProps(nextProps, nextContext) {
        const {devices} = this.props.data;
        if (devices.length !== nextProps.data.devices.length) {
            // If the length of next Devices is difference, is because one was created or deleted
            this.setState({devices: nextProps.data.devices})
        }
    }

    getImgUrl(item) {
        return 'https://maps.googleapis.com/maps/api/staticmap' +
            '?center=' + item.lat + ',' + item.lng + '&zoom=13&size=270x150&maptype=roadmap ' +
            '&markers=color:' + (item.state ? 'red' : 'blue') + '%7Clabel:Device%7C' + item.lat + ',' + item.lng +
            '&key=' + googleMapsApiKey
    }

    openEditModal(device) {
        const {devices} = this.props.data;
        const index = _.indexOf(devices, device);
        this.setState({editMode: true, deviceToEdit: device, modalOpen: true, indexOfEditedDevice: index})
    };

    deleteDevice(device) {
        const {devices} = this.props.data;
        const index = _.indexOf(devices, device);
        this.props.deleteDevice(index);
    }

    closeModal() {
        this.setState({modalOpen: false, editMode: false, deviceToEdit: {}, indexOfEditedDevice: -1})
    }

    filterDevicesByProp(prop, selectedValue) {
        const {devices} = this.props.data;
        const filteredDevices = _.filter(devices, [prop, selectedValue]);
        this.setState({devices: filteredDevices});
    }

    cleanFilters() {
        const {devices} = this.props.data;
        this.setState({devices: devices});
    }

    searchById(id) {
        const {devices} = this.props.data;
        const filteredDevice = _.filter(devices, ['id', parseInt(id)]);
        this.setState({devices: filteredDevice});
    }

    render() {
        const {newDevice, editDevice} = this.props;
        const {modalOpen, editMode, deviceToEdit, indexOfEditedDevice, devices} = this.state;

        const filterByZoneBnt = (
            <Dropdown overlay={
                <Menu onClick={(e) => this.filterDevicesByProp('zone', e.key)}>
                    <Menu.Item key="Sur">Sur</Menu.Item>
                    <Menu.Item key="Norte">Norte</Menu.Item>
                    <Menu.Item key="Centro">Centro</Menu.Item>
                </Menu>
            }>
                <Button style={{marginLeft: 8}}>
                    {'Select a zone to filter'} <Icon type="down"/>
                </Button>
            </Dropdown>
        );
        const filterByStateBnt = (
            <Dropdown overlay={
                <Menu onClick={(e) => this.filterDevicesByProp('state', parseInt(e.key))}>
                    <Menu.Item key="1">On</Menu.Item>
                    <Menu.Item key="0">Off</Menu.Item>
                </Menu>
            }>
                <Button style={{marginLeft: 8}}>
                    {'Select a state to filter'} <Icon type="down"/>
                </Button>
            </Dropdown>
        );
        const clearFilterBtn = (
            <Button style={{marginLeft: 8}} onClick={() => this.cleanFilters()}>
                <Icon type="sync"/>
            </Button>
        );
        const filterById = (
            <Input.Search
                placeholder="Search by ID"
                style={{width: '20%'}}
                onSearch={value => this.searchById(value)}
                enterButton
            />
        );

        return (
            <>
                <Alert
                    style={{width: '75%', margin: ' auto', marginTop: '0.5rem'}}
                    message="Requirement"
                    description="This page should include the list of all devices. The user should be able to filter by device name, states and zones."
                    type="info" showIcon closable/>

                <h1>List of devices</h1>

                <div style={{margin: '1rem', display: 'flex'}}>
                    <Button
                        icon="plus"
                        type="primary"
                        style={{marginRight: 'auto'}}
                        onClick={() => this.setState({modalOpen: true})}
                    >
                        New Device
                    </Button>

                    {filterById}
                    {filterByZoneBnt}
                    {filterByStateBnt}
                    {clearFilterBtn}
                </div>

                <div style={{width: '90%', margin: 'auto', marginTop: '1rem'}}>

                    <List
                        grid={{gutter: 16, column: 1, lg: 2}}
                        itemLayout="vertical"
                        size="large"
                        dataSource={devices}
                        renderItem={item => (
                            <List.Item
                                key={item}
                                actions={[
                                    <Icon onClick={() => this.deleteDevice(item)} type='delete'/>,
                                    <Icon onClick={() => this.openEditModal(item)} type='edit'/>
                                ]}
                                extra={
                                    <img width={270} alt="logo" src={this.getImgUrl(item)}/>}
                            >
                                <List.Item.Meta title={'Zona: ' + item.zone}/>
                                {'Device with ID ' + item.id + ' is currently ' + (item.state ? 'On' : 'Off')}
                            </List.Item>
                        )}
                    />
                </div>
                <DeviceModal
                    open={modalOpen}
                    device={deviceToEdit}
                    editMode={editMode}
                    onClose={() => this.closeModal()}
                    onSave={(device) => !editMode ? newDevice(device) : editDevice(device, indexOfEditedDevice)}/>
            </>
        );


    }

}

function DeviceModal(props) {

    const [lat, setLat] = useState(null);
    const [lng, setLng] = useState(null);
    const [state, setState] = useState(null);
    const [zone, setZone] = useState(null);

    if (props.editMode && props.device && !lat && !lng) {
        setLat(props.device.lat);
        setLng(props.device.lng);
        setState(props.device.state);
        setZone(props.device.zone);
    }

    const EditNewForm = (
        <Form>
            <Row gutter={24}>
                <Col span={12}>
                    <Form.Item label={'Lat'}>
                        <Input placeholder="Set Lat"
                               value={lat}
                               onChange={(e) => setLat(e.target.value)}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item label={'Lng'}>
                        <Input placeholder="Set lng"
                               value={lng}
                               onChange={(e) => setLng(e.target.value)}
                        />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item label={'Zone'}>
                        <Dropdown overlay={
                            <Menu onClick={(e) => setZone(e.key)}>
                                <Menu.Item key="Sur">Sur</Menu.Item>
                                <Menu.Item key="Norte">Norte</Menu.Item>
                                <Menu.Item key="Centro">Centro</Menu.Item>
                            </Menu>
                        }>
                            <Button style={{marginLeft: 8}}>
                                {zone || 'Select a zone '} <Icon type="down"/>
                            </Button>
                        </Dropdown>
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item label={'State'}>
                        <Dropdown overlay={
                            <Menu onClick={(e) => setState(e.key)}>
                                <Menu.Item key="1">On</Menu.Item>
                                <Menu.Item key="0">Off</Menu.Item>
                            </Menu>
                        }>
                            <Button style={{marginLeft: 8}}>
                                {state !== null
                                    ? state === '1' ? 'On' : 'Off'
                                    : 'Select a state '
                                } <Icon type="down"/>
                            </Button>
                        </Dropdown>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    );

    return (
        <Modal
            title={props.editMode ? 'Edit device' : 'Create new device'}
            centered
            visible={props.open}
            onOk={() => {
                props.onSave({lat: parseFloat(lat), lng: parseFloat(lng), state: parseInt(state), zone});
                props.onClose()
            }}
            onCancel={() => props.onClose()}
        >
            <Alert
                style={{width: '90%', margin: ' auto', marginTop: '0.5rem'}}
                message="Requirement"
                description="Lat and Lng, are not validated please enter correct values."
                type="warning" showIcon closable/>
            {EditNewForm}
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {...state};
};


export default withRouter(connect(mapStateToProps, {newDevice, editDevice, deleteDevice})(Detail));
