import GoogleMapReact from 'google-map-react';
import React from "react";
import {googleMapsApiKey} from "../env";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {Icon} from "antd";


export function MapView(props) {

    const defaultProps = {
        center: { // Lat and lng for the center of the map
            lat: -31.62,
            lng: -60.71
        },
        zoom: 13 // Default zoom for the map view - From 0 to 15
    };

    const Market = ({text, color}) => <Icon type="bulb" theme="twoTone" twoToneColor={color}/>;

    return (
        <div style={{height: '80%', width: '80%', margin: 'auto', marginTop: '1rem'}}>
            <GoogleMapReact
                bootstrapURLKeys={{key: googleMapsApiKey}}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
            >
                {props.data.devices.map( (item, index) =>
                    <Market
                        key={item + index}
                        lat={item.lat}
                        lng={item.lng}
                        color={item.state ? '#52c41a' : '#eb2f96'}
                    />
                )}
            </GoogleMapReact>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {...state};
};


export default withRouter(connect(mapStateToProps, {})(MapView));