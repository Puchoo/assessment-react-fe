import React, {useState} from "react";
import {connect} from "react-redux";
import _ from 'lodash';

import {Button, Divider, Dropdown, Icon, Menu} from "antd";
import GradientPie from "./GradientPie";

export function ChartView(props) {

    const menu = (
        <Menu onClick={(e) => setZone(e.key)}>
            <Menu.Item key="Sur">Sur</Menu.Item>
            <Menu.Item key="Norte">Norte</Menu.Item>
            <Menu.Item key="Centro">Centro</Menu.Item>
        </Menu>
    );

    const [selectedZone, setZone] = useState(null);
    // In the state property - 1 is On, 0 is Off
    const countDevices = _.countBy(props.data.devices, 'state');

    const getCountDevicesByZone = (state) => {
        let countDevicesByZone = {};
        const devicesByZone = _.filter(props.data.devices, ['zone', selectedZone]);
        countDevicesByZone = _.countBy(devicesByZone, 'state');
        return countDevicesByZone[state];
    };

    return (
        <>
            <GradientPie cantOn={countDevices[1]} cantOff={countDevices[0]}/>

            <Divider>Cant On/Off by Zone</Divider>
            <Dropdown overlay={menu}>
                <Button style={{marginLeft: 8}}>
                    {selectedZone || 'Select a zone '} <Icon type="down"/>
                </Button>
            </Dropdown>

            {selectedZone && <GradientPie cantOn={getCountDevicesByZone(1) || 0} cantOff={getCountDevicesByZone(0) || 0}/>}
        </>
    )
}

function mapStateToProps(state) {
    return {...state}
}

export default connect(mapStateToProps, {})(ChartView);