import React from 'react';
import {Cell, Pie, PieChart, Tooltip} from "recharts";
import {Card, Col, Icon, Row, Statistic} from "antd";

export default function GradientPie(props) {

    const data = [
        {name: 'Cant On', value: props.cantOn || 0}, {name: 'Cant Off', value: props.cantOff || 0},
    ];

    const COLORS = ['#00C49F', '#cf1322'];


    return (
        <Row gutter={16} type="flex" justify="space-around" align="middle">
            <Col span={6}>
                <Card>
                    <Statistic
                        title="Devices On"
                        value={props.cantOn}
                        valueStyle={{color: '#00C49F'}}
                        prefix={<Icon type="poweroff" />}
                    />
                </Card>
            </Col>
            <Col className="gutter-row" span={6}>
                <PieChart width={200} height={200}>
                    <Pie
                        dataKey="value"
                        isAnimationActive={true}
                        data={data}
                        cx={100}
                        cy={100}
                        outerRadius={80}
                        fill="#8884d8">
                        {data.map((entry, index) => <Cell key={entry} fill={COLORS[index % COLORS.length]}/>)}
                    </Pie>
                    <Tooltip/>
                </PieChart>
            </Col>
            <Col span={6}>
                <Card>
                    <Statistic
                        title="Devices Off"
                        value={props.cantOff}
                        valueStyle={{color: '#cf1322'}}
                        prefix={<Icon type="poweroff" />}
                    />
                </Card>
            </Col>
        </Row>
    );
}