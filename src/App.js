import React, {Component} from 'react';
import {Provider} from "react-redux";
import {Route, Router, Switch} from 'react-router-dom';

import PrivateRoute from "./components/commons/PrivateRoute";
import Header from "./components/commons/Header";
import Login from "./components/auth/Login";
import Home from "./components/Home";
import Details from "./components/Details";
import history from "./utils/history";
import {store} from "./redux/store/store";


import {apiKey, authDomain, databaseURL, messagingSenderId, projectId, storageBucket} from "./env";
import * as firebase from "firebase";

import './App.css';
import "antd/dist/antd.css";


class App extends Component {

  componentDidMount() {
    const config = {
      apiKey,
      authDomain,
      databaseURL,
      projectId,
      storageBucket,
      messagingSenderId
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
        <Provider store={store}>
          <Router history={history}>
            <div className="App">
              <Switch>
                <Route path="/" render={() => <Login/>} exact={true}/>
                <div>
                  <Header/>
                  <PrivateRoute path="/home" inner={Home}/>
                  <PrivateRoute path="/detail" inner={Details}/>
                </div>
              </Switch>
            </div>
          </Router>
        </Provider>
    );
  }
}

export default App;
