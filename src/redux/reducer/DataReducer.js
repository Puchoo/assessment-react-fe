import {
    FETCH_DEVICES,
    NEW_DEVICES,
    EDIT_DEVICE,
    DELETE_DEVICE
} from '../actions/types'

const INITIAL_STATE = {
    devices: [],
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case FETCH_DEVICES:
            return {...state, devices: action.payload};
        case NEW_DEVICES:
            return {...state, devices: [...state.devices, action.payload]};
        case EDIT_DEVICE:
            return {
                ...state,
                devices: state.devices.map((device, i) => i === action.payload.index ? action.payload.device : device)
            };
        case DELETE_DEVICE:
            return {
                ...state,
                devices: state.slice(0, action.payload).concat(state.slice(action.payload + 1, state.devices.length))
            };
        default:
            return state;
    }
};