import {
    LOGIN_WITH_GITHUB,
    LOGIN_WITH_GOOGLE,
    LOGOUT_USER,
} from "./types";
import Router from "../../utils/history";


export const loginWithGitHub = (token, user) => {
    return (dispatch) => {
        dispatch({type: LOGIN_WITH_GITHUB, payload: {token, user}});
        Router.push('/home');
    };
};

export const loginWithGoogle = (token, user) => {
    return (dispatch) => {
        dispatch({type: LOGIN_WITH_GOOGLE, payload: {token, user}});
        Router.push('/home');
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        dispatch({type: LOGOUT_USER});
        Router.push('/');
    }

};




