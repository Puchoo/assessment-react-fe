import {
    FETCH_DEVICES,
    NEW_DEVICES,
    EDIT_DEVICE,
    DELETE_DEVICE
} from "./types";
import axios from "axios";


export const fetchDevices = () => {
    return (dispatch) => {
        axios.get('http://localhost:3001/devices').then( (result) => {
            dispatch({type: FETCH_DEVICES, payload: result.data})
        })
    }
};

export const newDevice = (device) => {
    return (dispatch) => {
        // Device example = { "lat": -31.656403, "lng": -60.713565, "zone": "Norte", "state": 1 }
        axios.post('http://localhost:3001/devices',{...device}).then( (r) => {
            if (r.status === 201) dispatch({type: NEW_DEVICES, payload: r.data})
        })}
};

export const editDevice = (device, index) => {
    return (dispatch) => {
        dispatch({type: EDIT_DEVICE, payload: {device, index}})
    }
};

export const deleteDevice = (index) => {
    return (dispatch) => {
        dispatch({type: DELETE_DEVICE, payload: index})
    }
};

