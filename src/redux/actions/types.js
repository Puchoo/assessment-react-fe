/** ------------------- Login Actions --------------------**/

export const LOGIN_USER_FAILED = 'login_user_failed';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGIN_WITH_GITHUB = 'login_with_github';
export const LOGIN_WITH_GOOGLE = 'login_with_google';


/** ------------------- Data Actions --------------------**/

export const FETCH_DEVICES = 'fetch_devices';
export const NEW_DEVICES = 'new_devices';
export const EDIT_DEVICE = 'edit_device';
export const DELETE_DEVICE = 'delete_device';
